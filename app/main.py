import time, gc, network
from machine import Pin
from umqtt.simple import MQTTClient

gc.enable()

# wifi config and connection
WIFI_SSID='<SSID>'
WIFI_PW='<PW>'

sta_if = network.WLAN(network.STA_IF)
if not sta_if.isconnected():
    print('connecting to network...')
    sta_if.active(True)
    sta_if.connect(WIFI_SSID, WIFI_PW)
    while not sta_if.isconnected():
        pass
print('network config:', sta_if.ifconfig())

# mqtt config and connection
MQTT_SERVER="192.168.0.5"
MQTT_CLIENT_ID="esp-adelskronen"
MQTT_STAT_TOPIC = b"adelskronen/STAT"
MQTT_CTRL_TOPIC = b"adelskronen/POWER"

def sub_cb(topic, msg):
    global ctrl_pin
    if msg == b"TOGGLE":
        print("toggling power")
        ctrl_pin.value(0)
        time.sleep(0.1)
        ctrl_pin.value(1)

mqtt_client = MQTTClient(MQTT_CLIENT_ID, MQTT_SERVER)
mqtt_client.connect() 
mqtt_client.set_callback(sub_cb) 

mqtt_client.subscribe(MQTT_CTRL_TOPIC)

# pin config
STATUS_PIN = 14
stat_pin = Pin(STATUS_PIN, Pin.IN)

CTRL_PIN = 13
ctrl_pin = Pin(CTRL_PIN, Pin.OUT, value=1)

pwr_status = 0
while True:
    time.sleep(5)
    pwr_status = stat_pin.value()
    mqtt_client.publish(MQTT_STAT_TOPIC, str(pwr_status), retain=True)
    mqtt_client.check_msg()
